package com.ayotta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class EdiPersisterApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(EdiPersisterApplication.class);
        app.run();
	}
	
	
	public void run(String... args) throws Exception {
	    System.out.println("Enter the run");
	}
}
