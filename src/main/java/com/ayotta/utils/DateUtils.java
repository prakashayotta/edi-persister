package com.ayotta.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {
	private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);
	
	public static String startDate() {
		Date dt = new Date();
		dt.setTime(dt.getTime() - 3600 * 1000);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy HH:00:00");
		logger.debug("startDateFt : "+ formatter.format(dt));
		return formatter.format(dt);
	}
	
	public static String endDate() {
		Date dt = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy HH:00:00");
		logger.debug("endDateFt : "+ formatter.format(dt));
		return formatter.format(dt);
	}
}
