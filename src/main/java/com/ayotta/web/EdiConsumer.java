package com.ayotta.web;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.ayotta.edi.EDIOrderHeader;
import com.ayotta.edi.EdiService;
import com.ayotta.kafka.Producer;

@Service
public class EdiConsumer {

	@Autowired
	EdiService service;
	org.slf4j.Logger logger = LoggerFactory.getLogger(EdiConsumer.class);
	@Autowired
	private RestHighLevelClient client;
	
	@Autowired
	Producer producer;
	
	@PersistenceContext
	EntityManager em;
	
	@KafkaListener(topics = "edi", groupId = "group_id")
	public void consume(String message) {
		logger.info(String.format("$$ -> Consumed Message -> %s", message));
		try {
			EDIOrderHeader orderHeader = service.saveInDB(message,producer,em);
			logger.info(orderHeader.getMessageStatus());
//			logger.info(service.updateOrder(orderHeader));
		} catch (IOException e) {
			logger.error("Error in saved call ",e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in saved call ",e);
		}
	}
	
	/*EDI MESSAGE PROCEDURE CALL LISTENER*/ 
	@KafkaListener(topics = "edi_procedure_call", groupId = "sap")
	public void procedureCallConsume(String message) {
		try {
			logger.info(String.format("$$ -> Enter procedure call Message -> %s", message));
			EDIOrderHeader orderHeader = service.procedureCall(message,producer,em);
			logger.info(String.format("$$ -> Exit procedure call Message -> %s", orderHeader.getMessageStatus()));
		} catch (Exception e) {
			logger.error("Error in procedure call ",e);
		}
	}
}
