package com.ayotta.edi;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.ayotta.kafka.Producer;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class EdiService {
	
	@Autowired
	EDIOrderHeaderRepository headerRepository;

	@Autowired
	EDIOrderLineRepository orderLineRepository;

	private RestHighLevelClient esClient;

	@Autowired
	private ObjectMapper objectMapper;

	private Logger logger = LoggerFactory.getLogger(EdiService.class);

	private static String INDEX = "orders";

	private static String TYPE = "order";
	
	@Autowired
    private JavaMailSender emailSender;
	
	@Autowired
	MailProperties mailProperties;

	public String messageStatusStr = "READ_FROM_FILE";
	
	@Autowired
	public EdiService( RestHighLevelClient esClient) {

		this.esClient = esClient;
	}
	
	@Autowired
	XxmSpiceActionsRepo xxmSpiceActionsRepo;
	
	public EDIOrderHeader saveInDB(String message,Producer producer, EntityManager em) throws Exception {

		EDIOrderHeader orderHeader = convertStringToOrder(message);

		orderHeader.setCreationDate(new Date());
		orderHeader.setMessageStatus("SAVED_IN_DB");
		orderHeader = headerRepository.save(orderHeader);
		logger.info("Order header Id : "+ orderHeader.getOrderHeaderId());
		for (EDIOrderLine orderLine: orderHeader.getOrderLines()) {
			orderLine.setOrderHeaderId(orderHeader.getOrderHeaderId());
			if(orderLine.getOrdUnp() == null){
				orderLine.setOrdUnp(Double.valueOf(0));
			}
			orderLineRepository.save(orderLine);
		}
		try {
			logger.info(updateOrder(orderHeader));
		} catch (IOException e) {
			logger.error("Error in save DB",e);
			e.printStackTrace();
		} catch (Exception e) {
			logger.error("Error in save DB",e);
		}
		/*ObjectMapper mapper = new ObjectMapper();
		final String objectStringFinal =  mapper.writeValueAsString(orderHeader); 
		Thread newThread = new Thread(() -> {
			try {
				producer.sendMessage(objectStringFinal);	
			} catch (Exception e) {
				logger.error("Error in send kafka",e);
			}
		});*/
		
		if(orderHeader.getOrderHeaderId() >0) {
			processEdiOrders(orderHeader.getOrderHeaderId(),message,em);
			
			
		}
		/*UPDATE IN ELASTICSEARCH*/
		logger.debug("Processed EDI Order " + orderHeader.getOrderHeaderId());
		EDIOrderHeader headerMsg = headerRepository.findByOrderHeaderId(orderHeader.getOrderHeaderId());
		if(headerMsg != null){
			logger.info("header status:", headerMsg.toString());
			if(headerMsg.getStatus() != null){
				headerMsg.setMessageStatus("PROCESSED");
				headerRepository.save(headerMsg);
				logger.info(updateOrder(headerMsg));
			}
			//orderHeader.setOrderLines(orderLineRepository.findByOrderHeaderId(orderHeader.getOrderHeaderId()));
		}
		return orderHeader;
	}
	
	/*PROCEDURE CALL*/
	public EDIOrderHeader procedureCall(String message, Producer producer, EntityManager em) throws Exception {
		EDIOrderHeader orderHeader = convertStringToOrder(message);
		logger.debug("$$ -> edi message -> %s: "+ (orderHeader == null ? "" :orderHeader.getOrderHeaderId()));
		if(orderHeader != null){
			logger.debug("processingServiceRequest : "+orderHeader.getOrderHeaderId());
			processEdiOrders(orderHeader.getOrderHeaderId(),message,em);
			logger.debug("processingServiceRequestSuccess : "+orderHeader.getOrderHeaderId());
			
			/*UPDATE IN ELASTICSEARCH*/
			orderHeader = headerRepository.findByOrderHeaderId(orderHeader.getOrderHeaderId());
			if(orderHeader != null){
				logger.info("header status:", orderHeader.getStatus());
				if(orderHeader.getStatus() != null){
					orderHeader.setMessageStatus("PROCESSED");
					headerRepository.save(orderHeader);
					logger.info(updateOrder(orderHeader));
				}
			}
		}else{
			logger.debug("edi message is null");
		}
		return orderHeader;
	}
	
	public void processEdiOrders(Long headerId, String txtMessage, EntityManager em) {
		String source = "SAP";
		logger.debug("EntityManager : "+ em);
		logger.debug("process ediProcCall...");
			em.createNativeQuery("CALL ETA.XMP_PROCESS_EDI_ORDERS.process_mdsparesorder_in_edi(:source,:headerid)")
	        .setParameter("source", source).setParameter("headerid", headerId).executeUpdate();
	    logger.debug("processed ediProcCall...");
		boolean aogFlag= false;
		XxmSpiceActions action = new XxmSpiceActions();
		action.setExoHdrid(headerId);
		List<XxmSpiceActions> list = xxmSpiceActionsRepo.findByExoHdrid(action.getExoHdrid());
		if(list!=null && list.size()>0){
			for(XxmSpiceActions object:list){
				if(object.getAogFlag()!=null && object.getAogFlag().equals("Y")){
					aogFlag=true;
				}
			}
		}
		if(aogFlag){
			processAOG(txtMessage);
		}
	}

	public void processAOG(String txtMessage){
		MimeMessage message = emailSender.createMimeMessage();
    	try {
    		logger.debug("MailFrom: "+mailProperties.getMailFrom());
    		
    		MimeMessageHelper helper;
    		
    		helper = new MimeMessageHelper(message, true);
    		
    		helper.setFrom(mailProperties.getMailFrom());
    		if(mailProperties.getMailAogTo()!=null && !mailProperties.getMailAogTo().equals("")) {
    			
    			helper.setTo(mailProperties.getMailAogTo());
    		}else {
    			helper.setTo(mailProperties.getSchemaMailTo());
    		}
    		helper.setSubject("B2B MESSAGE: "
    				+ "WEB"
    				+ " INBOUND AOG MESSAGE @"
    				+ new SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
    				.format(new Date()));
    		helper.setText("The following AOG Message has arrived and will be sent to Oracle.\n"
    				+ txtMessage + "", false);
    		
    		emailSender.send(message);
    		logger.info("SENT_AOG_MAIL TO \t" + mailProperties.getMailAogTo() + ": ");
    		logger.info("Mail has been sent successfully...");
    	}catch (MailException exception) {
            exception.printStackTrace();
        } catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	/*private Session getSession() {
        // get session from entitymanager. Assuming hibernate
        return em.unwrap(org.hibernate.Session.class);
    }*/
	
	public String updateOrder(EDIOrderHeader document) throws Exception {
		EDIOrderHeader resultDocument = this.findById(document.getElasticSearchRef());
		UpdateRequest updateRequest = new UpdateRequest(INDEX, TYPE, resultDocument.getElasticSearchRef());
		updateRequest.doc(convertOrderToMap(document));
		UpdateResponse updateResponse = esClient.update(updateRequest, RequestOptions.DEFAULT);
		logger.debug("updateResponse : "+ updateResponse.getResult());
		return updateResponse.getResult().name();

	}

	public EDIOrderHeader findById(String id) throws Exception {
		GetRequest getRequest = new GetRequest(INDEX, TYPE, id);
		GetResponse getResponse = esClient.get(getRequest, RequestOptions.DEFAULT);
		Map<String, Object> resultMap = getResponse.getSource();
		return convertMapToOrder(resultMap);
	}
	
	
	private List<EDIOrderHeader> getSearchResult(SearchResponse response) {
		SearchHit[] searchHit = response.getHits().getHits();
		List<EDIOrderHeader> Orders = new ArrayList<>();
		for (SearchHit hit : searchHit) {
			Orders.add(objectMapper.convertValue(hit.getSourceAsMap(), EDIOrderHeader.class));
		}
		return Orders;
	}

	private Map<String, Object> convertOrderToMap(EDIOrderHeader Order) {
		return objectMapper.convertValue(Order, Map.class);
	}

	private EDIOrderHeader convertMapToOrder(Map<String, Object> map) {
		return objectMapper.convertValue(map, EDIOrderHeader.class);
	}

	private EDIOrderHeader convertStringToOrder(String map)
			throws JsonParseException, JsonMappingException, IOException {
		return objectMapper.readValue(map, EDIOrderHeader.class);
	}
}
