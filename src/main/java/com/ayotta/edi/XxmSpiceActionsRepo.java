package com.ayotta.edi;

import org.springframework.data.jpa.repository.JpaRepository;
import java.lang.Long;
import com.ayotta.edi.XxmSpiceActions;
import java.util.List;

public interface XxmSpiceActionsRepo extends JpaRepository<XxmSpiceActions, Long>{
	
	List<XxmSpiceActions> findByExoHdrid(Long exohdrid);

}