package com.ayotta.edi;

import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EDIOrderLineRepository extends JpaRepository<EDIOrderLine, Long>{

	Set<EDIOrderLine> findByOrderHeaderId(Long orderHeaderId);
}
