package com.ayotta.edi;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticsearchConfig {

    Logger logger = LoggerFactory.getLogger(ElasticsearchConfig.class);
	
	@Value("${elasticsearch.host}")
    public String elasticsearchHost;
	
	@Value("${elasticsearch.port}")
    public int elasticsearchPort;

    @Bean(destroyMethod = "close")
    public RestHighLevelClient client() {

    	logger.info("el port: "+elasticsearchPort+" "+"el host: " +elasticsearchHost );
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(elasticsearchHost,elasticsearchPort,"http")
                		));

        return client;

    }

	public String getElasticsearchHost() {
		return elasticsearchHost;
	}

	public int getElasticsearchPort() {
		return elasticsearchPort;
	}
}
