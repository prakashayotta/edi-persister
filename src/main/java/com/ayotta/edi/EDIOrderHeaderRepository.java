package com.ayotta.edi;

import org.springframework.data.jpa.repository.JpaRepository;


public interface EDIOrderHeaderRepository extends JpaRepository<EDIOrderHeader, Long>{

	EDIOrderHeader findByOrderHeaderId(Long orderHeaderId);
}
